# v0.5.0
- Replace unmaintained tui crate with ratatui
- Implement `add`, `help`, `new-playlist`, and `play` commands
- Add support for MPRIS media control support on Linux

# v0.4.2
- Fix double key press bug on Windows

# v0.4.1
- Fixed bug where "Select playlist" message displayed from the second character and would be appendend to the command line multiple times.
- Fixed command line cursor movement bug that caused characters to be shuffled around when moving the cursor left or right on the command line.

# v0.4.0
- Library now stored in .m3u8 file instead of nonstandard binary format. Libraries in old format will be automatically converted and re-saved in the new format.
- Fixed bug where only the first track in each album was showing up in "All Artists > *\[Album\]*".
- Add track length field to tracks pane.

# v0.3.1
- Fix clippy issues

# v0.3.0
- Each screen now has its own independent state which is preserved when moving between them.
- Added a help screen which displays keybindings.
- Implement vim-style command line which can be accessed by pressing ':'.
- Update Nerd Font icon characters to use new mappings (See Nerd Fonts v3.0.0 release notes for more information).
- Specify dependency versions to minor version precision in Cargo.toml to ensure greater stability. 

# v0.2.1
- Add prev/next track keybinds

# v0.2.0
- Added user configuration through yaml config file
- Added playlists

# v0.1.3
- Fixed bug where the next track in the queue is played when shuffle is turned off rather than the one after the current one

# v0.1.2
- Fixed bug causing panic when certain tracks with malformed metadata are played 

# v0.1.1
- Ensure that track progress doesn't exceed length and cause setting gauge ratio to panic
- Clear queue before enqueueing when user presses enter on an item so that it plays immediately
- When selecting from the Tracks panel, enqueue the selected track and every track after it, wrapping around if shuffle or repeat all is enabled
- Show currently playing track
- Fix extra time being added after pausing

# v0.1.0
Initial release