/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use std::path::PathBuf;

use anyhow::{anyhow, Result};

use crate::ui::ScreenEnum;

pub enum Command {
    Quit,
    Down,
    Up,
    NextPanel,
    PrevPanel,
    Play,
    Pause,
    Stop,
    TogglePlay,
    ToggleShuffle,
    ToggleRepeat,
    QueueAndPlay,
    GotoTop,
    GotoBottom,
    GotoScreen(ScreenEnum),
    NewPlaylist(Option<String>),
    PlaylistAdd,
    SelectPlaylist,
    PrevTrack,
    NextTrack,
    EnterCommand,
    AddPath(PathBuf),
    PlayTrack(PathBuf),
    Nop,
}

impl Command {
    pub fn parse(command: &str) -> Result<Self> {
        let mut tokens = command.split_whitespace();
        match tokens.next() {
            Some("q" | "quit" | "exit") => Ok(Command::Quit),
            Some("s" | "shuf" | "shuffle") => Ok(Command::ToggleShuffle),
            Some("r" | "rep" | "repeat") => Ok(Command::ToggleRepeat),
            Some("screen") => match tokens.next() {
                Some("1" | "main") => Ok(Command::GotoScreen(ScreenEnum::Main)),
                Some("2" | "playlist" | "playlists") => {
                    Ok(Command::GotoScreen(ScreenEnum::Playlists))
                }
                Some("0" | "help") => Ok(Command::GotoScreen(ScreenEnum::Help)),
                Some(other) => Err(anyhow!("screen: Invalid screen identifier: {}", other)),
                None => Err(anyhow!("screen: Missing argument SCREEN_ID")),
            },
            Some("h" | "help") => Ok(Command::GotoScreen(ScreenEnum::Help)),
            Some("a" | "add") => match command.split_once(' ') {
                Some((_, p)) => Ok(Command::AddPath(p.into())),
                None => Err(anyhow!("add: Missing argument PATH")),
            },
            Some("new-playlist" | "n") => match command.split_once(' ') {
                Some((_, name)) => Ok(Command::NewPlaylist(Some(name.into()))),
                None => Ok(Command::NewPlaylist(None)),
            },
            Some("play" | "p") => match command.split_once(' ') {
                Some((_, path)) => Ok(Command::PlayTrack(path.into())),
                None => Err(anyhow!("play: Missing argument PATH")),
            },
            Some(other) => Err(anyhow!("Invalid command: {}", other)),
            None => Ok(Command::Nop),
        }
    }
}
