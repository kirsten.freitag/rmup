/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use std::{fs::File, path::Path};

use anyhow::{anyhow, Result};
use crossterm::event::KeyCode;
use ratatui::style::Color;
use regex::Regex;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize)]
struct Colors {
    fg: String,
    highlight_fg: String,
    bg: String,
    highlight_bg: String,
    off_panel_hightlight: String,
}

#[derive(Serialize, Deserialize)]
struct Keybinds {
    up: Vec<String>,
    down: Vec<String>,
    left: Vec<String>,
    right: Vec<String>,
    play: Vec<String>,
    next: Vec<String>,
    prev: Vec<String>,
    queue: Vec<String>,
    repeat: Vec<String>,
    shuffle: Vec<String>,
    top: Vec<String>,
    bottom: Vec<String>,
    next_panel: Vec<String>,
    prev_panel: Vec<String>,
    main_screen: Vec<String>,
    help_screen: Vec<String>,
    playlist_screen: Vec<String>,
    new_playlist: Vec<String>,
    playlist_add: Vec<String>,
    select_playlist: Vec<String>,
    quit: Vec<String>,
}

#[derive(Serialize, Deserialize)]
struct Options {
    nerd_font_icons: bool,
}

#[derive(Serialize, Deserialize)]
struct ConfigSerde {
    colors: Colors,
    keybinds: Keybinds,
    options: Options,
}

pub struct Config {
    pub fg_color: Color,
    pub highlight_fg_color: Color,
    pub bg_color: Color,
    pub highlight_bg_color: Color,
    pub off_panel_highlight_color: Color,
    pub up_keys: Vec<KeyCode>,
    pub down_keys: Vec<KeyCode>,
    pub left_keys: Vec<KeyCode>,
    pub right_keys: Vec<KeyCode>,
    pub play_keys: Vec<KeyCode>,
    pub prev_keys: Vec<KeyCode>,
    pub next_keys: Vec<KeyCode>,
    pub queue_keys: Vec<KeyCode>,
    pub repeat_keys: Vec<KeyCode>,
    pub shuffle_keys: Vec<KeyCode>,
    pub top_keys: Vec<KeyCode>,
    pub bottom_keys: Vec<KeyCode>,
    pub next_panel_keys: Vec<KeyCode>,
    pub prev_panel_keys: Vec<KeyCode>,
    pub main_screen_keys: Vec<KeyCode>,
    pub playlist_screen_keys: Vec<KeyCode>,
    pub help_screen_keys: Vec<KeyCode>,
    pub new_playlist_keys: Vec<KeyCode>,
    pub playlist_add_keys: Vec<KeyCode>,
    pub select_playlist_keys: Vec<KeyCode>,
    pub quit_keys: Vec<KeyCode>,
    pub nerd_font_icons: bool,
}

impl Default for Config {
    fn default() -> Self {
        Self {
            fg_color: Color::White,
            highlight_fg_color: Color::Black,
            bg_color: Color::Reset,
            highlight_bg_color: Color::White,
            off_panel_highlight_color: Color::Red,
            up_keys: vec![KeyCode::Char('k'), KeyCode::Up],
            down_keys: vec![KeyCode::Char('j'), KeyCode::Down],
            left_keys: vec![KeyCode::Char('h'), KeyCode::Left],
            right_keys: vec![KeyCode::Char('l'), KeyCode::Right],
            play_keys: vec![KeyCode::Char(' ')],
            prev_keys: vec![KeyCode::Char(',')],
            next_keys: vec![KeyCode::Char('.')],
            queue_keys: vec![KeyCode::Enter],
            repeat_keys: vec![KeyCode::Char('r')],
            shuffle_keys: vec![KeyCode::Char('s')],
            top_keys: vec![KeyCode::Char('g')],
            bottom_keys: vec![KeyCode::Char('G')],
            next_panel_keys: vec![KeyCode::Tab],
            prev_panel_keys: vec![KeyCode::BackTab],
            main_screen_keys: vec![KeyCode::Char('1')],
            playlist_screen_keys: vec![KeyCode::Char('2')],
            help_screen_keys: vec![KeyCode::Char('0')],
            new_playlist_keys: vec![KeyCode::Char('n')],
            playlist_add_keys: vec![KeyCode::Char('p')],
            select_playlist_keys: vec![KeyCode::Char('x')],
            quit_keys: vec![KeyCode::Char('q')],
            nerd_font_icons: false,
        }
    }
}

impl Config {
    pub fn load<P: AsRef<Path>>(path: P) -> Result<Self> {
        let config_file = File::open(path)?;
        let loaded: ConfigSerde = serde_yaml::from_reader(config_file)?;
        Ok(Self {
            fg_color: parse_color(&loaded.colors.fg)?,
            highlight_fg_color: parse_color(&loaded.colors.highlight_fg)?,
            bg_color: parse_color(&loaded.colors.bg)?,
            highlight_bg_color: parse_color(&loaded.colors.highlight_bg)?,
            off_panel_highlight_color: parse_color(&loaded.colors.off_panel_hightlight)?,
            up_keys: parse_keys(&loaded.keybinds.up)?,
            down_keys: parse_keys(&loaded.keybinds.down)?,
            left_keys: parse_keys(&loaded.keybinds.left)?,
            right_keys: parse_keys(&loaded.keybinds.right)?,
            play_keys: parse_keys(&loaded.keybinds.play)?,
            prev_keys: parse_keys(&loaded.keybinds.prev)?,
            next_keys: parse_keys(&loaded.keybinds.next)?,
            queue_keys: parse_keys(&loaded.keybinds.queue)?,
            repeat_keys: parse_keys(&loaded.keybinds.repeat)?,
            shuffle_keys: parse_keys(&loaded.keybinds.shuffle)?,
            top_keys: parse_keys(&loaded.keybinds.top)?,
            bottom_keys: parse_keys(&loaded.keybinds.bottom)?,
            next_panel_keys: parse_keys(&loaded.keybinds.next_panel)?,
            prev_panel_keys: parse_keys(&loaded.keybinds.prev_panel)?,
            main_screen_keys: parse_keys(&loaded.keybinds.main_screen)?,
            playlist_screen_keys: parse_keys(&loaded.keybinds.playlist_screen)?,
            help_screen_keys: parse_keys(&loaded.keybinds.help_screen)?,
            new_playlist_keys: parse_keys(&loaded.keybinds.new_playlist)?,
            playlist_add_keys: parse_keys(&loaded.keybinds.playlist_add)?,
            select_playlist_keys: parse_keys(&loaded.keybinds.select_playlist)?,
            quit_keys: parse_keys(&loaded.keybinds.quit)?,
            nerd_font_icons: loaded.options.nerd_font_icons,
        })
    }

    pub fn save<P: AsRef<Path>>(&self, path: P) -> Result<()> {
        let config_file = File::create(path)?;
        let serializable = ConfigSerde {
            colors: Colors {
                fg: write_color(&self.fg_color),
                highlight_fg: write_color(&self.highlight_fg_color),
                bg: write_color(&self.bg_color),
                highlight_bg: write_color(&self.highlight_bg_color),
                off_panel_hightlight: write_color(&self.off_panel_highlight_color),
            },
            keybinds: Keybinds {
                up: write_keys(&self.up_keys),
                down: write_keys(&self.down_keys),
                left: write_keys(&self.left_keys),
                right: write_keys(&self.right_keys),
                play: write_keys(&self.play_keys),
                prev: write_keys(&self.prev_keys),
                next: write_keys(&self.next_keys),
                queue: write_keys(&self.queue_keys),
                repeat: write_keys(&self.repeat_keys),
                shuffle: write_keys(&self.shuffle_keys),
                top: write_keys(&self.top_keys),
                bottom: write_keys(&self.bottom_keys),
                next_panel: write_keys(&self.next_panel_keys),
                prev_panel: write_keys(&self.prev_panel_keys),
                main_screen: write_keys(&self.main_screen_keys),
                help_screen: write_keys(&self.help_screen_keys),
                playlist_screen: write_keys(&self.playlist_screen_keys),
                new_playlist: write_keys(&self.new_playlist_keys),
                playlist_add: write_keys(&self.playlist_add_keys),
                select_playlist: write_keys(&self.select_playlist_keys),
                quit: write_keys(&self.quit_keys),
            },
            options: Options {
                nerd_font_icons: self.nerd_font_icons,
            },
        };
        Ok(serde_yaml::to_writer(config_file, &serializable)?)
    }
}

fn parse_color(color_string: &str) -> Result<Color> {
    let re_hex1 = Regex::new(r"^#[0-9a-zA-Z]{6}$").unwrap();
    let re_hex2 = Regex::new(r"^0x[0-9a-zA-Z]{6}$").unwrap();
    Ok(match color_string {
        "Reset" => Color::Reset,
        "Black" => Color::Black,
        "Red" => Color::Red,
        "Green" => Color::Green,
        "Yellow" => Color::Yellow,
        "Blue" => Color::Blue,
        "Magenta" => Color::Magenta,
        "Cyan" => Color::Cyan,
        "Gray" => Color::Gray,
        "DarkGray" => Color::DarkGray,
        "LightRed" => Color::LightRed,
        "LightGreen" => Color::LightGreen,
        "LightYellow" => Color::LightYellow,
        "LightBlue" => Color::LightBlue,
        "LightMagenta" => Color::LightMagenta,
        "LightCyan" => Color::LightCyan,
        "White" => Color::White,
        _ => {
            if re_hex1.is_match(color_string) {
                let red: u8 = color_string[1..=2].parse()?;
                let green: u8 = color_string[3..=4].parse()?;
                let blue: u8 = color_string[5..=6].parse()?;
                Color::Rgb(red, green, blue)
            } else if re_hex2.is_match(color_string) {
                let red: u8 = color_string[2..=3].parse()?;
                let green: u8 = color_string[4..=5].parse()?;
                let blue: u8 = color_string[6..=7].parse()?;
                Color::Rgb(red, green, blue)
            } else if let Ok(index) = color_string.parse() {
                Color::Indexed(index)
            } else {
                return Err(anyhow!("Color: '{color_string}' is not a supported format"));
            }
        }
    })
}

fn write_color(color: &Color) -> String {
    match color {
        Color::Reset => "Reset".to_owned(),
        Color::Black => "Black".to_owned(),
        Color::Red => "Red".to_owned(),
        Color::Green => "Green".to_owned(),
        Color::Yellow => "Yellow".to_owned(),
        Color::Blue => "Blue".to_owned(),
        Color::Magenta => "Magenta".to_owned(),
        Color::Cyan => "Cyan".to_owned(),
        Color::Gray => "Gray".to_owned(),
        Color::DarkGray => "DarkGray".to_owned(),
        Color::LightRed => "LightRed".to_owned(),
        Color::LightGreen => "LightGreen".to_owned(),
        Color::LightYellow => "LightYellow".to_owned(),
        Color::LightBlue => "LightBlue".to_owned(),
        Color::LightMagenta => "LightMagenta".to_owned(),
        Color::LightCyan => "LightCyan".to_owned(),
        Color::White => "White".to_owned(),
        Color::Rgb(r, g, b) => format!("#{r:02x}{g:02x}{b:02x}"),
        Color::Indexed(i) => i.to_string(),
    }
}

fn parse_keys(keys_strings: &[String]) -> Result<Vec<KeyCode>> {
    let fn_re = Regex::new(r"^F[0-24]$")?;
    let mut keys = Vec::new();
    for ks in keys_strings.iter() {
        if ks.chars().collect::<Vec<char>>().len() == 1 {
            keys.push(KeyCode::Char(ks.chars().next().unwrap()));
        } else if fn_re.is_match(ks) {
            let n: u8 = ks[1..].parse()?;
            keys.push(KeyCode::F(n));
        } else {
            keys.push(match ks.as_str() {
                "Backspace" => KeyCode::Backspace,
                "Enter" => KeyCode::Enter,
                "Left" => KeyCode::Left,
                "Right" => KeyCode::Right,
                "Up" => KeyCode::Up,
                "Down" => KeyCode::Down,
                "Home" => KeyCode::Home,
                "End" => KeyCode::End,
                "PageUp" => KeyCode::PageUp,
                "PageDown" => KeyCode::PageDown,
                "Tab" => KeyCode::Tab,
                "BackTab" => KeyCode::BackTab,
                "Delete" => KeyCode::Delete,
                "Insert" => KeyCode::Insert,
                "Null" => KeyCode::Null,
                "Esc" => KeyCode::Esc,
                "CapsLock" => KeyCode::CapsLock,
                "ScrollLock" => KeyCode::ScrollLock,
                "NumLock" => KeyCode::NumLock,
                "PrintScreen" => KeyCode::PrintScreen,
                "Pause" => KeyCode::Pause,
                "Menu" => KeyCode::Menu,
                "KeypadBegin" => KeyCode::KeypadBegin,
                "Space" => KeyCode::Char(' '),
                _ => return Err(anyhow!("Key '{ks}' not recognized")),
            });
        }
    }
    Ok(keys)
}

fn write_keys(keys: &[KeyCode]) -> Vec<String> {
    keys.iter()
        .filter_map(|k| match k {
            KeyCode::Backspace => Some("Backspace".to_owned()),
            KeyCode::Enter => Some("Enter".to_owned()),
            KeyCode::Left => Some("Left".to_owned()),
            KeyCode::Right => Some("Right".to_owned()),
            KeyCode::Up => Some("Up".to_owned()),
            KeyCode::Down => Some("Down".to_owned()),
            KeyCode::Home => Some("Home".to_owned()),
            KeyCode::End => Some("End".to_owned()),
            KeyCode::PageUp => Some("PageUp".to_owned()),
            KeyCode::PageDown => Some("PageDown".to_owned()),
            KeyCode::Tab => Some("Tab".to_owned()),
            KeyCode::BackTab => Some("BackTab".to_owned()),
            KeyCode::Delete => Some("Delete".to_owned()),
            KeyCode::Insert => Some("Insert".to_owned()),
            KeyCode::Null => Some("Null".to_owned()),
            KeyCode::Esc => Some("Esc".to_owned()),
            KeyCode::CapsLock => Some("CapsLock".to_owned()),
            KeyCode::ScrollLock => Some("ScrollLock".to_owned()),
            KeyCode::NumLock => Some("NumLock".to_owned()),
            KeyCode::PrintScreen => Some("PrintScreen".to_owned()),
            KeyCode::Pause => Some("Pause".to_owned()),
            KeyCode::Menu => Some("Menu".to_owned()),
            KeyCode::KeypadBegin => Some("KeypadBegin".to_owned()),
            KeyCode::Char(' ') => Some("Space".to_owned()),
            KeyCode::Char(c) => Some(c.to_string()),
            KeyCode::F(n) => Some(format!("F{n}")),
            _ => None,
        })
        .collect()
}
