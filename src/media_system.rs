/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use std::{
    collections::VecDeque,
    fs::File,
    io::BufReader,
    mem,
    sync::{Arc, Mutex},
    time::Duration,
};

use crate::{library::Track, panicmsg};
use anyhow::Result;
use rodio::{Decoder, OutputStream, OutputStreamHandle, Sink};

#[cfg(target_os = "linux")]
use crate::mpris::MprisPlayer;
#[cfg(target_os = "linux")]
use mpris_server::{LoopStatus, Metadata, PlaybackStatus, Property, Server, Time};

#[derive(PartialEq, Eq, Copy, Clone)]
pub enum Repeat {
    On,
    Off,
    One,
}

#[derive(Default)]
pub struct MediaState {
    pub current_track: Option<Track>,
    pub current_track_progress: Option<Duration>,
    pub playing: bool,
    pub stopped: bool,
    pub shuffle: bool,
    pub repeat: Repeat,
}

pub struct MediaSystem {
    state: Arc<Mutex<MediaState>>,
    #[cfg(target_os = "linux")]
    mpris_server: Arc<Mutex<Server<MprisPlayer>>>,
    sink: Sink,
    stream_handle: OutputStreamHandle,
    _stream: OutputStream,
    queue: VecDeque<Track>,
    history: Vec<Track>,
}

impl Default for Repeat {
    fn default() -> Self {
        Self::Off
    }
}

impl MediaSystem {
    pub async fn new(
        #[cfg(target_os = "linux")] mpris_server: Arc<Mutex<Server<MprisPlayer>>>,
        state: Arc<Mutex<MediaState>>,
    ) -> Result<Self> {
        let (_stream, stream_handle) = OutputStream::try_default()?;
        let sink = Sink::try_new(&stream_handle)?;
        #[cfg(target_os = "linux")]
        {
            let server = mpris_server.lock().expect(panicmsg::MPRIS_SERVER_LOCK);
            server
                .properties_changed([
                    Property::CanSeek(false),
                    Property::Metadata(Metadata::new()),
                    Property::PlaybackStatus(PlaybackStatus::Stopped),
                    Property::LoopStatus(LoopStatus::None),
                    Property::Shuffle(false),
                ])
                .await?;
        }

        Ok(Self {
            state,
            #[cfg(target_os = "linux")]
            mpris_server,
            stream_handle,
            sink,
            _stream,
            queue: VecDeque::new(),
            history: Vec::new(),
        })
    }

    /// Get the current state of the MediaSystem
    pub fn state(&self) -> &Arc<Mutex<MediaState>> {
        &self.state
    }

    /// Add a track to the play queue
    pub fn enqueue(&mut self, track: &Track) {
        self.queue.push_back(track.clone());
    }

    /// If there is a current track and it is paused, resume it. Otherwise does
    /// nothing.
    pub async fn play(&mut self) {
        let mut guard = self.state.lock().expect(panicmsg::MEDIA_STATE_LOCK);
        if guard.current_track.is_some() && !guard.playing {
            guard.playing = true;
            guard.stopped = false;
            self.sink.play();
        }
        #[cfg(target_os = "linux")]
        {
            let server = self.mpris_server.lock().expect(panicmsg::MPRIS_SERVER_LOCK);
            let _ = server
                .properties_changed([Property::PlaybackStatus(PlaybackStatus::Playing)])
                .await;
        }
    }

    /// If there is a current track and it is playing, pause it. Otherwise does
    /// nothing.
    pub async fn pause(&mut self) {
        let mut guard = self.state.lock().expect(panicmsg::MEDIA_STATE_LOCK);
        if guard.current_track.is_some() && guard.playing {
            guard.playing = false;
            self.sink.pause();
        }
        #[cfg(target_os = "linux")]
        {
            let server = self.mpris_server.lock().expect(panicmsg::MPRIS_SERVER_LOCK);
            let _ = server
                .properties_changed([Property::PlaybackStatus(PlaybackStatus::Paused)])
                .await;
        }
    }

    pub async fn stop(&mut self) -> Result<()> {
        let mut guard = self.state.lock().expect(panicmsg::MEDIA_STATE_LOCK);
        if !self.sink.empty() {
            self.sink.stop();
            guard.stopped = true;
            self.sink = Sink::try_new(&self.stream_handle)?;
        }

        #[cfg(target_os = "linux")]
        {
            let server = self.mpris_server.lock().expect(panicmsg::MPRIS_SERVER_LOCK);
            server
                .properties_changed([Property::PlaybackStatus(PlaybackStatus::Stopped)])
                .await?;
        }

        Ok(())
    }

    pub async fn play_track(&mut self, track: &Track) -> Result<()> {
        self.stop().await?;

        let file = BufReader::new(File::open(&track.file_path)?);
        let source = Decoder::new(file)?;
        let mut guard = self.state.lock().expect(panicmsg::MEDIA_STATE_LOCK);

        guard.current_track = Some(track.clone());
        guard.current_track_progress = Some(Duration::from_millis(0));
        guard.playing = true;
        self.sink.append(source);

        #[cfg(target_os = "linux")]
        {
            let mut metadata_builder = Metadata::builder()
                .title(track.title.clone().unwrap_or(track.file_path.clone()))
                .artist([&track.artist])
                .album(&track.album)
                .length(Time::from_secs(track.length.as_secs() as i64));
            if let Some(number) = track.number {
                metadata_builder = metadata_builder.track_number(number as i32);
            }
            let server = self.mpris_server.lock().expect(panicmsg::MPRIS_SERVER_LOCK);
            let _ = server
                .properties_changed([
                    Property::PlaybackStatus(PlaybackStatus::Playing),
                    Property::Metadata(metadata_builder.build()),
                ])
                .await;
        }
        Ok(())
    }

    /// Play the next track in the queue
    pub async fn play_next(&mut self) -> Result<()> {
        let mut guard = self.state.lock().expect(panicmsg::MEDIA_STATE_LOCK);

        let next_track = if guard.repeat == Repeat::One {
            guard.current_track.clone()
        } else {
            if guard.shuffle && !self.queue.is_empty() {
                let index = rand::random::<usize>() % self.queue.len();
                self.queue.rotate_left(index);
            }
            self.queue.pop_front()
        };

        if let Some(track) = next_track {
            if let Some(current_track) = mem::take(&mut guard.current_track) {
                self.history.push(current_track.clone());
                if guard.repeat == Repeat::On {
                    self.queue.push_back(current_track)
                }
            }
            drop(guard);
            self.play_track(&track).await?;
        }

        Ok(())
    }

    pub async fn play_prev(&mut self) -> Result<()> {
        let guard = self.state.lock().expect(panicmsg::MEDIA_STATE_LOCK);

        if let Some(prev_track) = self.history.pop() {
            if let Some(current_track) = &guard.current_track {
                self.queue.push_front(current_track.clone());
            }
            drop(guard);
            self.play_track(&prev_track).await?;
        } else if let Some(current_track) = guard.current_track.clone() {
            drop(guard);
            self.play_track(&current_track).await?;
        }

        Ok(())
    }

    pub async fn enqueue_and_play(&mut self, tracks: Vec<Track>) -> Result<()> {
        self.queue.clear();
        tracks.iter().for_each(|t| self.enqueue(t));
        if let Some(track) = self.queue.pop_front() {
            self.play_track(&track).await
        } else {
            Ok(())
        }
    }

    /// Add the given duration to the current track's playback progress
    pub fn update_progress(&mut self, duration: Duration) {
        let mut guard = self.state.lock().expect(panicmsg::MEDIA_STATE_LOCK);

        if let Some(progress) = guard.current_track_progress.as_mut() {
            *progress += duration;
        }

        if self.sink_empty() {
            guard.playing = false;
            guard.current_track_progress = None;
            if guard.repeat != Repeat::One {
                guard.current_track = None;
            }
        }
    }

    /// Toggle between playing/paused
    pub async fn toggle_play(&mut self) {
        let guard = self.state.lock().expect(panicmsg::MEDIA_STATE_LOCK);
        let status = if guard.current_track.is_some() {
            if guard.playing {
                drop(guard);
                self.pause().await;
                #[cfg(target_os = "linux")]
                PlaybackStatus::Paused
            } else {
                drop(guard);
                self.play().await;
                #[cfg(target_os = "linux")]
                PlaybackStatus::Playing
            }
        } else {
            #[cfg(target_os = "linux")]
            PlaybackStatus::Stopped
        };
        #[cfg(target_os = "linux")]
        {
            let server = self.mpris_server.lock().expect(panicmsg::MPRIS_SERVER_LOCK);
            let _ = server
                .properties_changed([Property::PlaybackStatus(status)])
                .await;
        }
    }

    pub async fn toggle_shuffle(&mut self) {
        let mut guard = self.state.lock().expect(panicmsg::MEDIA_STATE_LOCK);
        guard.shuffle = !guard.shuffle;

        #[cfg(target_os = "linux")]
        {
            let server = self.mpris_server.lock().expect(panicmsg::MPRIS_SERVER_LOCK);
            let _ = server
                .properties_changed([Property::Shuffle(guard.shuffle)])
                .await;
        }
    }

    pub async fn toggle_repeat(&mut self) {
        use Repeat::*;
        let mut guard = self.state.lock().expect(panicmsg::MEDIA_STATE_LOCK);

        guard.repeat = match guard.repeat {
            Off => One,
            One => On,
            On => Off,
        };

        #[cfg(target_os = "linux")]
        {
            let server = self.mpris_server.lock().expect(panicmsg::MPRIS_SERVER_LOCK);
            let _ = server
                .properties_changed([Property::LoopStatus(guard.repeat.into())])
                .await;
        }
    }

    pub fn sink_empty(&self) -> bool {
        self.sink.empty()
    }

    pub fn queue_empty(&self) -> bool {
        self.queue.is_empty()
    }

    pub fn clear_queue(&mut self) {
        self.queue.clear()
    }
}

#[cfg(target_os = "linux")]
impl From<Repeat> for LoopStatus {
    fn from(val: Repeat) -> Self {
        match val {
            Repeat::On => LoopStatus::Playlist,
            Repeat::Off => LoopStatus::None,
            Repeat::One => LoopStatus::Track,
        }
    }
}
