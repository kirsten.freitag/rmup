/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

pub const COMMAND_QUEUE_LOCK: &str = "Could not get a lock on command queue";
pub const MEDIA_STATE_LOCK: &str = "Could not get a lock on media state";
pub const MPRIS_SERVER_LOCK: &str = "Could not get a lock on MPRIS server";
