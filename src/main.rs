/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use std::{
    collections::VecDeque,
    env, fs, io,
    path::Path,
    process,
    sync::{Arc, Mutex},
    time::{Duration, SystemTime},
};

use anyhow::{anyhow, Result};
use crossterm::{
    event::{self, Event, KeyCode, KeyEventKind},
    execute,
    terminal::{disable_raw_mode, enable_raw_mode, EnterAlternateScreen, LeaveAlternateScreen},
};
use getopts::Options;
use media_system::MediaSystem;
use ratatui::{backend::CrosstermBackend, Terminal};

#[cfg(target_os = "linux")]
use mpris_server::Server;

mod command;
mod config;
mod library;
mod media_system;
mod panicmsg;
mod playlist;
mod traits;
mod ui;
mod util;

#[cfg(target_os = "linux")]
mod mpris;

use library::*;
use traits::*;
use ui::UI;

use crate::{
    command::Command, config::Config, media_system::MediaState, playlist::Playlist,
    ui::MovementDirection,
};

#[cfg(target_os = "linux")]
use crate::mpris::MprisPlayer;

pub enum Mode {
    Normal,
    PlaylistEntry,
    CommandEntry,
}

#[cfg(target_os = "linux")]
const BUS_NAME: &str = "xyz.jcheatum.RMuP";

#[async_std::main]
async fn main() -> Result<()> {
    let argv: Vec<String> = env::args().collect();
    let prog = &argv[0];
    let mut opts = Options::new();
    opts.optopt("c", "config", "Specify config file location", "FILE");
    opts.optopt("a", "add", "Add a directory to library", "DIR");
    opts.optopt("l", "lib", "Use the given library file", "FILE");
    opts.optflag("h", "help", "print usage and exit");
    let matches = match opts.parse(&argv[1..]) {
        Ok(m) => m,
        Err(f) => {
            eprintln!("{}: Error: {}", prog, f);
            process::exit(1);
        }
    };
    if matches.opt_present("h") {
        print_usage(prog, opts);
        process::exit(0);
    }

    let data_dir = dirs_next::data_dir().unwrap().join("rmup");
    if !data_dir.exists() {
        fs::create_dir(&data_dir)?;
    }

    let config_dir = dirs_next::config_dir().unwrap().join("rmup");
    if !config_dir.exists() {
        fs::create_dir(&config_dir)?;
    }

    let lib_file_path = data_dir.join("library.m3u8");
    let old_lib_path = data_dir.join("library.cbor");
    let mut lib = if matches.opt_present("l") {
        let path = matches
            .opt_str("l")
            .ok_or(anyhow!("Option '-l' requires an argument"))?;
        if let Ok(lib) = Playlist::load(&path) {
            lib
        } else {
            let old_lib = Library::load(&path)?;
            Playlist::from(old_lib)
        }
    } else if lib_file_path.exists() {
        Playlist::load(&lib_file_path)?
    } else if old_lib_path.exists() {
        let old_lib = Library::load(old_lib_path)?;
        let lib = Playlist::from(old_lib);
        lib.save(&lib_file_path)?;
        lib
    } else {
        Playlist::new("Library")
    };
    lib.tracks.sort();

    if matches.opt_present("a") {
        let path = matches
            .opt_str("a")
            .ok_or(anyhow!("Option '-a' requires an argument"))?;
        library::add_path(&mut lib, path)?;
        lib.tracks.sort();
        lib.save(&lib_file_path)?;
    }

    let config_file_path = config_dir.join("config.yaml");
    let config = if matches.opt_present("c") {
        let path = matches
            .opt_str("c")
            .ok_or(anyhow!("Option '-c' requires an argument"))?;
        Config::load(path)?
    } else if config_file_path.exists() {
        Config::load(config_file_path)?
    } else {
        let c = Config::default();
        c.save(config_file_path)?;
        c
    };

    let playlist_dir = data_dir.join("playlists");
    if !Path::new(&playlist_dir).exists() {
        fs::create_dir(&playlist_dir)?;
    }
    let playlists: Vec<Playlist> = fs::read_dir(&playlist_dir)?
        .filter_map(|entry| {
            if let Ok(entry) = entry {
                if let Some(ext) = entry.path().extension() {
                    match ext.to_str() {
                        Some("m3u8") | Some("m3u") => Some(entry.path()),
                        _ => None,
                    }
                } else {
                    None
                }
            } else {
                None
            }
        })
        .filter_map(|p| Playlist::load(p).ok())
        .collect();

    enable_raw_mode()?;
    let mut stdout = io::stdout();
    execute!(stdout, EnterAlternateScreen)?;
    let backend = CrosstermBackend::new(stdout);
    let mut terminal = Terminal::new(backend)?;

    let mut app_ui = UI::new(&lib, &config, playlists);
    let state = Arc::new(Mutex::new(MediaState::default()));
    let command_queue = Arc::new(Mutex::new(VecDeque::<Command>::new()));
    #[cfg(target_os = "linux")]
    let server = Arc::new(Mutex::new(
        Server::new(
            BUS_NAME,
            MprisPlayer::new(command_queue.clone(), state.clone()),
        )
        .await?,
    ));
    let mut media_system = MediaSystem::new(
        #[cfg(target_os = "linux")]
        server,
        state,
    )
    .await?;

    use command::Command::*;
    use ui::MovementDirection::*;

    let result: Result<()>;
    let poll_duration = Duration::from_millis(100);
    let mut time = SystemTime::now();
    let mut mode = Mode::Normal;

    loop {
        app_ui.draw(&mut terminal, media_system.state(), &config, &mode)?;

        if event::poll(poll_duration)? {
            if let Event::Key(ke) = event::read()? {
                if ke.kind == KeyEventKind::Press || ke.kind == KeyEventKind::Repeat {
                    match (&mode, ke.code) {
                        // Standard UI interaction
                        (Mode::Normal, _) => {
                            let mut guard =
                                command_queue.lock().expect(panicmsg::COMMAND_QUEUE_LOCK);
                            guard.push_back(app_ui.get_key_command(ke, &config))
                        }

                        // Entering name of new playlist at command line
                        (Mode::PlaylistEntry | Mode::CommandEntry, KeyCode::Char(c)) => {
                            app_ui.command_line.push(c);
                            app_ui.command_line.move_cursor(MovementDirection::Next);
                        }

                        (Mode::PlaylistEntry | Mode::CommandEntry, KeyCode::Backspace) => {
                            app_ui.command_line.backspace()
                        }

                        (Mode::PlaylistEntry | Mode::CommandEntry, KeyCode::Delete) => {
                            app_ui.command_line.delete()
                        }

                        (Mode::PlaylistEntry, KeyCode::Enter) => {
                            let playlist_name = app_ui.command_line.get_contents();
                            let playlist = Playlist::new(&playlist_name);
                            app_ui.add_playlist(&playlist);
                            playlist.save(playlist_dir.join(format!("{}.m3u8", playlist.name)))?;
                            app_ui.command_line.reset();
                            mode = Mode::Normal;
                        }

                        (Mode::CommandEntry, KeyCode::Enter) => {
                            let command = app_ui.command_line.get_contents();
                            app_ui.command_line.reset();
                            match Command::parse(&command) {
                                Ok(cmd) => {
                                    let mut guard =
                                        command_queue.lock().expect(panicmsg::COMMAND_QUEUE_LOCK);
                                    guard.push_back(cmd);
                                }
                                Err(e) => app_ui.command_line.push_str(format!("{}", e).as_str()),
                            }
                            mode = Mode::Normal;
                        }

                        (Mode::PlaylistEntry | Mode::CommandEntry, KeyCode::Esc) => {
                            app_ui.command_line.reset();
                            mode = Mode::Normal;
                        }

                        (Mode::PlaylistEntry | Mode::CommandEntry, KeyCode::Left) => {
                            app_ui.command_line.move_cursor(MovementDirection::Prev)
                        }

                        (Mode::PlaylistEntry | Mode::CommandEntry, KeyCode::Right) => {
                            app_ui.command_line.move_cursor(MovementDirection::Next)
                        }

                        _ => {}
                    }
                }
            }
        }

        let mut guard = command_queue.lock().expect(panicmsg::COMMAND_QUEUE_LOCK);
        if let Some(cmd) = guard.pop_front() {
            drop(guard);
            match cmd {
                Quit => {
                    result = Ok(());
                    break;
                }
                Down => {
                    app_ui.switch_item(Next);
                    app_ui.update_lists();
                }
                Up => {
                    app_ui.switch_item(Prev);
                    app_ui.update_lists();
                }
                NextPanel => app_ui.switch_panel(Next),
                PrevPanel => app_ui.switch_panel(Prev),
                Play => {
                    media_system.play().await;
                    time = SystemTime::now();
                }
                Pause => {
                    media_system.pause().await;
                }
                Stop => {
                    media_system.stop().await?;
                    media_system.clear_queue();
                }
                TogglePlay => {
                    media_system.toggle_play().await;
                    time = SystemTime::now();
                }
                ToggleShuffle => media_system.toggle_shuffle().await,
                ToggleRepeat => media_system.toggle_repeat().await,
                QueueAndPlay => {
                    let tracks = app_ui.get_selected_tracks(false);
                    media_system.enqueue_and_play(tracks).await?;
                    time = SystemTime::now();
                }
                GotoTop => app_ui.switch_item(Top),
                GotoBottom => app_ui.switch_item(Bottom),
                GotoScreen(s) => app_ui.switch_screen(s),
                NewPlaylist(None) => {
                    mode = Mode::PlaylistEntry;
                    app_ui.command_line.clear_contents();
                    app_ui.command_line.set_prompt("New playlist: ");
                }
                NewPlaylist(Some(playlist_name)) => {
                    let playlist = Playlist::new(&playlist_name);
                    app_ui.add_playlist(&playlist);
                    playlist.save(playlist_dir.join(format!("{}.m3u8", playlist.name)))?;
                }
                PlaylistAdd => {
                    app_ui.add_selected_to_playlist();
                    if let Some(pl) = app_ui.selected_playlist() {
                        pl.save(playlist_dir.join(format!("{}.m3u8", pl.name)))?;
                    }
                }
                SelectPlaylist => app_ui.select_current_playlist(),
                PrevTrack => media_system.play_prev().await?,
                NextTrack => media_system.play_next().await?,
                EnterCommand => {
                    mode = Mode::CommandEntry;
                    app_ui.command_line.reset();
                    app_ui.command_line.set_prompt(":")
                }
                AddPath(p) => {
                    let mut l = app_ui.library.clone();
                    match library::add_path(&mut l, p) {
                        Ok(_) => {
                            l.tracks.sort();
                            l.save(&lib_file_path)?;
                            app_ui.update_library(l);
                        }
                        Err(e) => {
                            app_ui.command_line.push_str(e.to_string().as_str());
                        }
                    }
                }
                PlayTrack(path) => {
                    let (track, _, _) = get_track_data(path)?;
                    media_system.play_track(&track).await?;
                    time = SystemTime::now();
                }
                Nop => {}
            }
        }

        if media_system
            .state()
            .lock()
            .expect(panicmsg::MEDIA_STATE_LOCK)
            .playing
        {
            media_system.update_progress(time.elapsed()?);
            time = SystemTime::now();
        }

        if media_system.sink_empty() && !media_system.queue_empty() {
            media_system.play_next().await?;
            time = SystemTime::now();
        }

        app_ui.update_lists();
    }

    // Restore terminal
    disable_raw_mode()?;
    execute!(terminal.backend_mut(), LeaveAlternateScreen)?;
    terminal.show_cursor()?;

    result
}

fn print_usage(program: &str, opts: Options) {
    let brief = format!("Usage: {} [options]", program);
    print!("{}", opts.usage(&brief));
}
