/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use anyhow::Result;
use crossterm::event::{KeyCode, KeyEvent};
use ratatui::{
    backend::Backend,
    layout::{Constraint, Direction, Layout, Rect},
    style::{Modifier, Style},
    text::{Line, Span, Text},
    widgets::{Block, Borders, Gauge, List, ListItem, ListState, Paragraph},
    Frame, Terminal,
};
use std::{
    cmp::Ordering,
    mem,
    rc::Rc,
    sync::{Arc, Mutex},
};

use crate::{
    command::Command,
    config::Config,
    library::Track,
    media_system::{MediaState, Repeat},
    panicmsg,
    playlist::Playlist,
    Mode,
};

mod command_line;
mod help_screen;
mod main_screen;
mod playlist_screen;

use command_line::*;
use help_screen::*;
use main_screen::*;
use playlist_screen::*;

pub enum MovementDirection {
    Prev,
    Next,
    Top,
    Bottom,
}

#[derive(PartialEq, Eq)]
pub enum ScreenEnum {
    Main,
    Playlists,
    Help,
}

trait Screen {
    fn ui(&mut self, f: &mut Frame, page_chunk: &Rect);
    fn style_panels(&mut self, selected: &Style, unselected: &Style);
    fn switch_panel(&mut self, direction: MovementDirection);
    fn switch_item(&mut self, direction: MovementDirection);
    fn update_lists(&mut self, normal_style: &Style);
    fn get_selected_tracks(&self, tracks_current_only: bool) -> Vec<Track>;
}

pub struct UIList<'a, Item> {
    /// The items in the list
    list: Vec<Item>,

    /// The TUI List widget
    display: List<'a>,

    /// The List widget state
    state: ListState,
}

pub struct UI<'a> {
    main_screen: MainScreen<'a>,

    playlist_screen: PlaylistScreen<'a>,

    help_screen: HelpScreen<'a>,

    /// Playback progress bar
    playback_bar: Gauge<'a>,

    /// The current screen
    screen: ScreenEnum,

    /// Base widget style
    normal_style: Style,

    /// Highlight style for the currently selected panel
    highlight_selected: Style,

    /// Highlight style for the unselected panels
    highlight_unselected: Style,

    selected_playlist_index: Option<usize>,

    pub library: Playlist,

    pub command_line: CommandLine,
}

const NF_PLAY: char = '\u{f040a}';
const NF_PAUSE: char = '\u{f03e4}';
const NF_SHUFFLE: char = '\u{f049d}';
const NF_SHUFFLE_OFF: char = '\u{f049e}';
const NF_REPEAT: char = '\u{f0456}';
const NF_REPEAT_OFF: char = '\u{f0457}';
const NF_REPEAT_ONCE: char = '\u{f0458}';

impl<'a> UI<'a> {
    /// Create a new UI object, constructing the artist, album, and track lists
    /// from the given library.
    pub fn new(library: &'a Playlist, config: &'a Config, playlists: Vec<Playlist>) -> Self {
        use ScreenEnum::*;

        let normal_style = Style::default().bg(config.bg_color).fg(config.fg_color);
        let highlight_selected = Style::default()
            .bg(config.highlight_bg_color)
            .fg(config.highlight_fg_color);
        let highlight_unselected = Style::default()
            .bg(config.bg_color)
            .fg(config.off_panel_highlight_color);

        let playback_bar = Gauge::default()
            .block(Block::default().borders(Borders::ALL))
            .gauge_style(normal_style)
            .ratio(0.0)
            .label("--:--/--:--");

        // Construct and configure UI
        let mut ui = Self {
            main_screen: MainScreen::new(library, &normal_style),
            playlist_screen: PlaylistScreen::new(&playlists, &normal_style),
            help_screen: HelpScreen::new(config, &normal_style),
            playback_bar,
            screen: Main,
            normal_style,
            highlight_selected,
            highlight_unselected,
            selected_playlist_index: None,
            library: library.clone(),
            command_line: CommandLine::default(),
        };

        ui.style_panels();
        ui
    }

    pub fn update_library(&mut self, library: Playlist) {
        self.main_screen = MainScreen::new(&library, &self.normal_style);
        self.library = library;
    }

    /// Set the selection highlight for each panel based on which one is
    /// currently selected.
    fn style_panels(&mut self) {
        match self.screen {
            ScreenEnum::Main => self
                .main_screen
                .style_panels(&self.highlight_selected, &self.highlight_unselected),
            ScreenEnum::Playlists => self
                .playlist_screen
                .style_panels(&self.highlight_selected, &self.highlight_unselected),
            ScreenEnum::Help => self
                .help_screen
                .style_panels(&self.highlight_selected, &self.highlight_unselected),
        }
    }

    /// Build the UI and draw it to the terminal
    pub fn draw<B: Backend>(
        &mut self,
        terminal: &mut Terminal<B>,
        media_state: &Arc<Mutex<MediaState>>,
        config: &Config,
        mode: &Mode,
    ) -> Result<()> {
        use ScreenEnum::*;

        terminal.draw(|f| {
            let chunks = Layout::default()
                .direction(Direction::Vertical)
                .constraints(
                    [
                        Constraint::Min(3),
                        Constraint::Length(3),
                        Constraint::Length(1),
                    ]
                    .as_ref(),
                )
                .split(f.size());
            match &self.screen {
                Main => self.main_screen.ui(f, &chunks[0]),
                Playlists => self.playlist_screen.ui(f, &chunks[0]),
                Help => self.help_screen.ui(f, &chunks[0]),
            }
            let playback_chunk = Layout::default()
                .direction(Direction::Horizontal)
                .constraints([Constraint::Length(26), Constraint::Min(3)].as_ref())
                .split(chunks[1]);
            self.render_playback_area(f, media_state, config, playback_chunk);
            let cursor = match mode {
                Mode::Normal => false,
                Mode::PlaylistEntry | Mode::CommandEntry => true,
            };
            self.render_command_line(f, chunks[2], cursor);
        })?;

        Ok(())
    }

    fn render_playback_area(
        &mut self,
        f: &mut Frame,
        media_state: &Arc<Mutex<MediaState>>,
        config: &Config,
        playback_chunk: Rc<[Rect]>,
    ) {
        let guard = media_state.lock().expect(panicmsg::MEDIA_STATE_LOCK);
        let playback_info = format!(
            " {} {} {} | {}",
            match guard.repeat {
                Repeat::On =>
                    if config.nerd_font_icons {
                        NF_REPEAT
                    } else {
                        'R'
                    },
                Repeat::Off =>
                    if config.nerd_font_icons {
                        NF_REPEAT_OFF
                    } else {
                        '-'
                    },
                Repeat::One =>
                    if config.nerd_font_icons {
                        NF_REPEAT_ONCE
                    } else {
                        '1'
                    },
            },
            if guard.shuffle {
                if config.nerd_font_icons {
                    NF_SHUFFLE
                } else {
                    'S'
                }
            } else if config.nerd_font_icons {
                NF_SHUFFLE_OFF
            } else {
                '-'
            },
            if guard.playing {
                if config.nerd_font_icons {
                    NF_PLAY
                } else {
                    '>'
                }
            } else if config.nerd_font_icons {
                NF_PAUSE
            } else {
                '-'
            },
            if let Some(track) = &guard.current_track {
                if let Some(title) = &track.title {
                    title
                } else {
                    &track.file_path
                }
            } else {
                "Not Playing"
            }
        );
        let info_text = Text::from(playback_info);
        let info_widget = Paragraph::new(info_text)
            .block(Block::default().borders(Borders::ALL))
            .style(self.normal_style);
        self.playback_bar = mem::take(&mut self.playback_bar)
            .label(format!(
                "{}:{}/{}:{}",
                // Minutes into playback
                if let Some(duration) = guard.current_track_progress {
                    format!("{:02}", duration.as_secs() / 60)
                } else {
                    "--".to_owned()
                },
                // Seconds into playback
                if let Some(duration) = guard.current_track_progress {
                    format!("{:02}", duration.as_secs() % 60)
                } else {
                    "--".to_owned()
                },
                // Track length minutes
                if let Some(track) = &guard.current_track {
                    format!("{:02}", track.length.as_secs() / 60)
                } else {
                    "--".to_owned()
                },
                // Track length seconds
                if let Some(track) = &guard.current_track {
                    format!("{:02}", track.length.as_secs() % 60)
                } else {
                    "--".to_owned()
                },
            ))
            .ratio(
                if let (Some(progress), Some(track)) =
                    (guard.current_track_progress, &guard.current_track)
                {
                    let ratio = progress.as_secs_f64() / track.length.as_secs_f64();
                    if ratio < 0.0 || ratio.is_nan() {
                        0.0
                    } else if ratio > 1.0 {
                        1.0
                    } else {
                        ratio
                    }
                } else {
                    0.0
                },
            );

        f.render_widget(info_widget, playback_chunk[0]);
        f.render_widget(self.playback_bar.clone(), playback_chunk[1]);
    }

    fn render_command_line(&mut self, f: &mut Frame, command_line_chunk: Rect, show_cursor: bool) {
        let mut before_cursor = String::new();
        let mut cursor = String::new();
        let mut after_cursor = String::new();
        let mut total_bytes = 0;

        for c in self.command_line.contents.chars() {
            match total_bytes.cmp(&self.command_line.cursor_pos) {
                Ordering::Less => before_cursor.push(c),
                Ordering::Equal => cursor.push(c),
                _ => after_cursor.push(c),
            }
            total_bytes += c.len_utf8();
        }

        let cmd_text = Line::from(vec![
            Span::styled(
                &self.command_line.prompt,
                self.normal_style.add_modifier(Modifier::BOLD),
            ),
            Span::raw(before_cursor),
            if show_cursor {
                Span::styled(cursor, self.normal_style.add_modifier(Modifier::REVERSED))
            } else {
                Span::raw(cursor)
            },
            Span::raw(after_cursor),
            Span::styled(
                " ",
                if self.command_line.cursor_pos == self.command_line.contents.len() && show_cursor {
                    self.normal_style.add_modifier(Modifier::REVERSED)
                } else {
                    self.normal_style
                },
            ),
        ]);

        let cmd_widget = Paragraph::new(cmd_text).style(self.normal_style);
        f.render_widget(cmd_widget, command_line_chunk);
    }

    /// Move the selection up or down the list in the current panel, cycling
    /// back around after the beginning or end of the list.
    pub fn switch_item(&mut self, direction: MovementDirection) {
        match self.screen {
            ScreenEnum::Main => self.main_screen.switch_item(direction),
            ScreenEnum::Playlists => self.playlist_screen.switch_item(direction),
            ScreenEnum::Help => self.help_screen.switch_item(direction),
        }
    }

    /// Switch to the next panel.
    pub fn switch_panel(&mut self, direction: MovementDirection) {
        match self.screen {
            ScreenEnum::Main => self.main_screen.switch_panel(direction),
            ScreenEnum::Playlists => self.playlist_screen.switch_panel(direction),
            ScreenEnum::Help => self.help_screen.switch_panel(direction),
        }
        self.style_panels();
    }

    pub fn switch_screen(&mut self, screen: ScreenEnum) {
        if self.screen != screen {
            self.screen = screen;
            self.update_lists();
        }
    }

    /// Change which album and track lists will be shown in the UI based on
    /// which artist and album list items are selected.
    pub fn update_lists(&mut self) {
        match self.screen {
            ScreenEnum::Main => self.main_screen.update_lists(&self.normal_style),
            ScreenEnum::Playlists => self.playlist_screen.update_lists(&self.normal_style),
            ScreenEnum::Help => self.help_screen.update_lists(&self.normal_style),
        }

        // Ensure panels are styled correctly after replacing them
        self.style_panels();
    }

    /// If artist is selected, return the artist track list. If album is
    /// selected, return the album track list. If track is selected, return a
    /// Vec containing just that track if tracks_current_only == true, or
    /// otherwise a Vec containing all of the tracks in the list starting from
    /// the current track and wrapping around at the end of the list.
    pub fn get_selected_tracks(&self, tracks_current_only: bool) -> Vec<Track> {
        match self.screen {
            ScreenEnum::Main => self.main_screen.get_selected_tracks(tracks_current_only),
            ScreenEnum::Playlists => self
                .playlist_screen
                .get_selected_tracks(tracks_current_only),
            ScreenEnum::Help => self.help_screen.get_selected_tracks(tracks_current_only),
        }
    }

    /// Return the command that corresponds to the given input.
    pub fn get_key_command(&self, ke: KeyEvent, config: &Config) -> Command {
        use Command::*;

        if config.quit_keys.contains(&ke.code) {
            Quit
        } else if config.down_keys.contains(&ke.code) {
            Down
        } else if config.up_keys.contains(&ke.code) {
            Up
        } else if config.next_panel_keys.contains(&ke.code) {
            NextPanel
        } else if config.prev_panel_keys.contains(&ke.code) {
            PrevPanel
        } else if config.queue_keys.contains(&ke.code) {
            QueueAndPlay
        } else if config.play_keys.contains(&ke.code) {
            TogglePlay
        } else if config.prev_keys.contains(&ke.code) {
            PrevTrack
        } else if config.next_keys.contains(&ke.code) {
            NextTrack
        } else if config.repeat_keys.contains(&ke.code) {
            ToggleRepeat
        } else if config.shuffle_keys.contains(&ke.code) {
            ToggleShuffle
        } else if config.top_keys.contains(&ke.code) {
            GotoTop
        } else if config.bottom_keys.contains(&ke.code) {
            GotoBottom
        } else if config.main_screen_keys.contains(&ke.code) {
            GotoScreen(ScreenEnum::Main)
        } else if config.playlist_screen_keys.contains(&ke.code) {
            GotoScreen(ScreenEnum::Playlists)
        } else if config.help_screen_keys.contains(&ke.code) {
            GotoScreen(ScreenEnum::Help)
        } else if config.new_playlist_keys.contains(&ke.code)
            && self.screen == ScreenEnum::Playlists
        {
            NewPlaylist(None)
        } else if config.playlist_add_keys.contains(&ke.code) {
            PlaylistAdd
        } else if config.select_playlist_keys.contains(&ke.code)
            && self.screen == ScreenEnum::Playlists
            && self.playlist_screen.panel == playlist_screen::Panel::Playlists
        {
            SelectPlaylist
        } else if ke.code == KeyCode::Char(':') {
            EnterCommand
        } else {
            Nop
        }
    }

    pub fn add_playlist(&mut self, playlist: &Playlist) {
        self.playlist_screen
            .playlist_list
            .list
            .push(playlist.clone());
        let listitems: Vec<ListItem> = self
            .playlist_screen
            .playlist_list
            .list
            .iter()
            .map(|pl| ListItem::new(pl.name.clone()))
            .collect();
        let list_display = List::new(listitems)
            .block(Block::default().title("Playlists").borders(Borders::ALL))
            .style(self.normal_style);
        self.playlist_screen.playlist_list.display = list_display;
    }

    pub fn add_selected_to_playlist(&mut self) {
        if let Some(index) = self.selected_playlist_index {
            let mut tracks = self.get_selected_tracks(true);
            self.playlist_screen.playlist_list.list[index].add(&mut tracks)
        } else {
            let message = "Select a playlist to add tracks";
            self.command_line.reset();
            self.command_line.push_str(message);
            self.command_line.cursor_pos = message.len();
        }
    }

    /// Set the currently highlighted item in the playlist list to the selected
    /// playlist.
    pub fn select_current_playlist(&mut self) {
        if let Some(new_index) = self.playlist_screen.playlist_list.state.selected() {
            let listitems: Vec<ListItem> = self
                .playlist_screen
                .playlist_list
                .list
                .iter()
                .enumerate()
                .map(|(i, p)| {
                    if i == new_index {
                        ListItem::new(format!("*{}", p.name))
                    } else {
                        ListItem::new(p.name.clone())
                    }
                })
                .collect();
            let list_display = List::new(listitems)
                .block(Block::default().title("Playlists").borders(Borders::ALL))
                .style(self.normal_style);
            self.playlist_screen.playlist_list.display = list_display;
            self.selected_playlist_index = Some(new_index)
        }
    }

    pub fn selected_playlist(&self) -> Option<&Playlist> {
        if let Some(index) = self.selected_playlist_index {
            Some(&self.playlist_screen.playlist_list.list[index])
        } else {
            None
        }
    }
}
