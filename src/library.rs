/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use anyhow::{anyhow, Result};
use lofty::{Accessor, AudioFile, Probe, TaggedFileExt};
use ratatui::widgets::ListItem;
use serde::{Deserialize, Serialize};
use std::{
    cmp::Ordering,
    fs::{self, File},
    path::Path,
    time::Duration,
};

use crate::{playlist::Playlist, traits::*, util::*};

#[derive(Clone, Default, Serialize, Deserialize, Debug, Eq, PartialEq)]
pub struct Artist {
    /// Name of the artist
    pub name: String,

    /// All of the albums in the library by this artist. The very first item
    /// in the list should be a pseudo-album named "All Albums" whose list of
    /// tracks contains all of the tracks by this artist.
    pub albums: Vec<Album>,
}

#[derive(Clone, Default, Serialize, Deserialize, Debug, Eq, PartialEq)]
pub struct Album {
    /// Name of the album
    pub name: String,

    /// Year from metadata, if present in tracks
    pub year: Option<u32>,

    /// Tracks in the album.
    pub tracks: Vec<Track>,
}

#[derive(Clone, Serialize, Deserialize, Debug, Eq, PartialEq)]
pub struct Track {
    /// Track name from metadata, if no name is present, filename will be
    /// displayed instead
    pub title: Option<String>,

    /// Artist name from metadata if present
    pub artist: String,

    /// Album name from metadata if present
    pub album: String,

    /// Year from metadata if present
    pub year: Option<u32>,

    /// Track number if present
    pub number: Option<u32>,

    /// Track duration
    pub length: Duration,

    /// Path to the audio file
    pub file_path: String,
}

/// Old library format for backwards compatibility. DEPRECATED AS OF v0.4.0.
/// WILL BE REMOVED IN v1.0.0. DO NOT USE IN NEW CODE!
#[derive(Serialize, Deserialize, Debug)]
pub struct Library {
    /// All of the artists in the library. The very first item in the list
    /// should be a pseudo-artist named "All Artists" whose list of albums
    /// contains all of the albums in the library.
    pub artists: Vec<Artist>,

    /// All of the albums in the library. The very first item in the list should
    /// be a pseudo-album named "All Albums" whose list of tracks contains all
    /// of the tracks in the library.
    pub albums: Vec<Album>,

    /// All of the tracks in the library.
    pub tracks: Vec<Track>,
}

impl Artist {
    pub fn name(mut self, name: &str) -> Self {
        self.name = name.to_owned();
        self
    }
}

impl Album {
    pub fn name(mut self, name: &str) -> Self {
        self.name = name.to_owned();
        self
    }

    pub fn year(mut self, year: Option<u32>) -> Self {
        self.year = year;
        self
    }
}

impl Save for Library {
    /// Write the library to a CBOR file
    fn save<P: AsRef<Path>>(&self, file_path: P) -> Result<()> {
        let lib_file = File::create(file_path)?;
        Ok(ciborium::ser::into_writer(self, lib_file)?)
    }
}

impl Load for Library {
    /// Read library from a CBOR file
    fn load<P: AsRef<Path>>(file_path: P) -> Result<Self> {
        let lib_file = File::open(file_path)?;
        Ok(ciborium::de::from_reader(lib_file)?)
    }
}

impl Artist {
    pub fn get_album_index(&self, name: &str) -> Option<usize> {
        self.albums.iter().position(|a| a.name == name)
    }
}

pub fn get_track_data<P: AsRef<Path>>(path: P) -> Result<(Track, Artist, Album)> {
    let path = path.as_ref();
    if !path.is_file() {
        return Err(anyhow!("{} is not a file", path.display()));
    }

    let tagged_file = Probe::open(path)?.read()?;

    let track = if let Some(tag) = tagged_file.primary_tag() {
        Track {
            title: tag.title().as_deref().map(|s| s.to_owned()),
            artist: tag.artist().as_deref().unwrap_or("Unknown").to_owned(),
            album: tag.album().as_deref().unwrap_or("Unknown").to_owned(),
            year: tag.year(),
            number: tag.track(),
            length: tagged_file.properties().duration(),
            file_path: path.to_str().unwrap().to_string(),
        }
    } else {
        Track {
            title: None,
            artist: "Unknown".to_owned(),
            album: "Unknown".to_owned(),
            year: None,
            number: None,
            length: tagged_file.properties().duration(),
            file_path: path.to_str().unwrap().to_string(),
        }
    };

    let mut artist = Artist::default().name(track.artist.as_str());

    let mut album = Album::default().name(track.album.as_str()).year(track.year);

    album.tracks.push(track.clone());

    artist.albums.push(Album::default().name("All Albums"));
    artist.albums[0].tracks.push(track.clone());
    artist.albums.push(album.clone());

    Ok((track, artist, album))
}

/// Artists sort alphabetically
impl Ord for Artist {
    fn cmp(&self, other: &Self) -> Ordering {
        if self.name == "All Artists" && other.name != "All Artists" {
            Ordering::Less
        } else if other.name == "All Artists" {
            Ordering::Greater
        } else {
            self.name.to_lowercase().cmp(&other.name.to_lowercase())
        }
    }
}

impl PartialOrd for Artist {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

/// Albums sort alphabetically
impl Ord for Album {
    fn cmp(&self, other: &Self) -> Ordering {
        if self.name == "All Albums" && other.name != "All Albums" {
            Ordering::Less
        } else if other.name == "All Albums" {
            Ordering::Greater
        } else {
            self.name.to_lowercase().cmp(&other.name.to_lowercase())
        }
    }
}

impl PartialOrd for Album {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl<'a> Into<ListItem<'a>> for &Album {
    fn into(self) -> ListItem<'a> {
        let title = self.name.clone();
        let year = match self.year {
            Some(y) => y.to_string(),
            None => "".to_owned(),
        };
        let term_width = crossterm::terminal::size().unwrap_or((80, 24)).0 as usize;
        let extra = term_width % 2;
        let box_width = term_width / 2 + extra - 2;
        let title_width = box_width - 4;
        ListItem::new(format!("{}{}", to_width(&title, title_width, false), year))
    }
}

/// Tracks sort first by artist. If they have the same artist, then they sort by
/// album. If they're on the same album, they then sort by track number. If
/// track number is not applicable to one or both of them, then they sort by
/// title. If title is not applicable to one or both of them, then the filename
/// is substituted for the title.
impl Ord for Track {
    fn cmp(&self, other: &Self) -> Ordering {
        if self.artist != other.artist {
            self.artist.cmp(&other.artist)
        } else if self.album != other.album {
            self.album.cmp(&other.album)
        } else if let (Some(self_num), Some(other_num)) = (self.number, other.number) {
            self_num.cmp(&other_num)
        } else {
            let self_name = self
                .title
                .as_ref()
                .unwrap_or(&self.file_path)
                .to_lowercase();
            let other_name = other
                .title
                .as_ref()
                .unwrap_or(&other.file_path)
                .to_lowercase();
            self_name.cmp(&other_name)
        }
    }
}

impl PartialOrd for Track {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl<'a> Into<ListItem<'a>> for &Track {
    fn into(self) -> ListItem<'a> {
        let title = self.title.as_ref().unwrap_or(&self.file_path);
        let artist = &self.artist;
        let album = &self.album;
        let year = match self.year {
            Some(y) => y.to_string(),
            None => "".to_owned(),
        };
        let length = format!(
            "{}:{:02}",
            self.length.as_secs() / 60,
            self.length.as_secs() % 60
        );

        let box_width = crossterm::terminal::size()
            .unwrap_or((80, 24))
            .0
            .saturating_sub(2) as usize;

        let col = box_width / 5;
        let col_widths = match box_width % 5 {
            0 => (col, col, col, col, col),
            1 => (col + 1, col, col, col, col),
            2 => (col + 1, col + 1, col, col, col),
            3 => (col + 1, col + 1, col + 1, col, col),
            4 => (col + 1, col + 1, col + 1, col + 1, col),
            _ => panic!(), // not mathematically possible
        };

        ListItem::new(format!(
            "{}{}{}{}{}",
            to_width(title, col_widths.0, false),
            to_width(artist, col_widths.1, false),
            to_width(album, col_widths.2, false),
            to_width(&year, col_widths.3, true),
            to_width(&length, col_widths.4, true),
        ))
    }
}

//-----------------------------New Library Code---------------------------------

pub fn add_path<P: AsRef<Path>>(library: &mut Playlist, path: P) -> Result<()> {
    let path = path.as_ref();
    if !path.exists() {
        return Err(anyhow!("{}: No such file or directory", path.display()));
    }
    if !path.is_dir() {
        if let Some(ext) = path.extension() {
            let ext = ext.to_string_lossy().into_owned();
            match ext.as_str() {
                "mp3" | "flac" | "aiff" | "m4a" | "ogg" | "opus" | "aac" | "wav" => {}
                _ => {
                    return Ok(());
                }
            }
        } else {
            return Ok(());
        }
        let (track, _, _) = get_track_data(path)?;

        // Add track to library
        library.tracks.push(track.clone());
    } else {
        for entry in fs::read_dir(path)? {
            let entry = entry?;
            add_path(library, entry.path())?;
        }
    }

    Ok(())
}
