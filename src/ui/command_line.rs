/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use crate::ui::MovementDirection;

#[derive(Default, Debug)]
pub struct CommandLine {
    /// Text displayed before the command line
    pub prompt: String,

    /// Text in the command line at the bottom of the screen
    pub contents: String,

    /// Current position of the cursor in the command string
    pub cursor_pos: usize,
}

impl CommandLine {
    /// Set the command prompt to the given string
    pub fn set_prompt(&mut self, prompt: &str) {
        self.prompt = prompt.to_owned();
    }

    pub fn clear_prompt(&mut self) {
        self.prompt.clear();
    }

    /// Append a char to the command line
    pub fn push(&mut self, c: char) {
        self.contents.push(c);
    }

    /// Append a string to the command line
    pub fn push_str(&mut self, s: &str) {
        self.contents.push_str(s);
    }

    /// Delete one character to the left of the cursor on the command line
    pub fn backspace(&mut self) {
        if self.cursor_pos > 0 {
            self.move_cursor(MovementDirection::Prev);
            self.delete();
        }
    }

    /// Delete the character under the cursor on the command line
    pub fn delete(&mut self) {
        if !self.contents.is_empty() && self.cursor_pos < self.contents.len() {
            self.contents.remove(self.cursor_pos);
        }
    }

    /// Move the cursor along the command line
    pub fn move_cursor(&mut self, direction: MovementDirection) {
        match direction {
            MovementDirection::Next => {
                if self.cursor_pos < self.contents.len() {
                    self.cursor_pos += 1;
                    while !self.contents.is_char_boundary(self.cursor_pos)
                        && self.cursor_pos < self.contents.len()
                    {
                        self.cursor_pos += 1;
                    }
                }
            }
            MovementDirection::Prev => {
                if self.cursor_pos > 0 {
                    self.cursor_pos -= 1;
                    while !self.contents.is_char_boundary(self.cursor_pos) {
                        self.cursor_pos -= 1;
                    }
                }
            }
            MovementDirection::Top => self.cursor_pos = 0,
            MovementDirection::Bottom => {
                self.cursor_pos = self.contents.len() - 1;
                while !self.contents.is_char_boundary(self.cursor_pos) {
                    self.cursor_pos -= 1;
                }
            }
        }
    }

    /// Clear the command line
    pub fn clear_contents(&mut self) {
        self.contents.clear();
    }

    /// Get the current contents of the command line string
    pub fn get_contents(&self) -> String {
        self.contents.clone()
    }

    /// Reset all fields to their default values
    pub fn reset(&mut self) {
        self.clear_contents();
        self.clear_prompt();
        self.cursor_pos = 0;
    }
}
